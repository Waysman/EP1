#include "pretobranco.hpp"
#include <iostream>
#include <fstream>

Pretobranco::Pretobranco(){

}
Pretobranco::~Pretobranco(){

}
void Pretobranco::AplicarFiltro(){
    *getSaidaImagem() << getIdentificador() << endl;
    
    *getSaidaImagem() << getLargura() << " " << getAltura() << endl;
    
    *getSaidaImagem() << getMaximaCor()<< endl;

    int grayscale_value;
    for(int i = 0; i < getAltura(); i++){
        for(int j = 0; j < getLargura(); j++){
           grayscale_value = (0.299 * getPixel(i, j, 'r')) + (0.587 * getPixel(i, j, 'g')) + (0.114 *getPixel(i, j, 'b'));

           *getSaidaImagem() << (unsigned char)grayscale_value;
           *getSaidaImagem() << (unsigned char)grayscale_value;
           *getSaidaImagem() << (unsigned char)grayscale_value;
        }
    }
    
}