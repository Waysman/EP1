#include "media.hpp"
#include <iostream>
#include <fstream>

Media::Media(){

}
Media::~Media(){

}
void Media::AplicarFiltro(int filter_size){
    *getSaidaImagem() << getIdentificador() << endl;    
    *getSaidaImagem() << getLargura() << " " << getAltura() << endl;
    *getSaidaImagem() << getMaximaCor()<< endl;
    Media::setFiltersize(filter_size);

    

    int limit=filter_size/2;
    unsigned int valuered;
    unsigned int valuegreen;
    unsigned int valueblue;
    for(int i = limit; i < (getAltura()-limit); i++){
        for(int j =limit; j < (getLargura()-limit); j++) {
            valuered = 0;
            valuegreen = 0;
            valueblue = 0;
            for(int y=-limit; y <= limit; y++) {
                 for(int x=-limit; x<=limit;x++){

                    valuered += getPixel(i+y, j+x, 'r');
                    valuegreen += getPixel(i+y, j+x, 'g');
                    valueblue += getPixel(i+y, j+x, 'b');
                            
                        
                }
            }

            setPixel(i, j, 'r', (valuered/(filter_size*filter_size)));
            setPixel(i, j, 'g', (valuegreen/(filter_size*filter_size)));
            setPixel(i, j, 'b', (valueblue/(filter_size*filter_size)));      
        }

    }

    for (int i=limit;i<getAltura();i++){
       for(int j=0;j<getLargura();j++){
           *getSaidaImagem() << (unsigned char)getPixel(i, j, 'r');
           *getSaidaImagem() << (unsigned char)getPixel(i, j, 'g');
           *getSaidaImagem() << (unsigned char)getPixel(i, j, 'b');

       }

   }

}
int Media::getFiltersize(){
    return filter_size;
}
void Media::setFiltersize(int filter_size){
    this->filter_size=filter_size;
}