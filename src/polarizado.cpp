#include "polarizado.hpp"
#include <iostream>
#include <fstream>

Polarizado::Polarizado(){
}
Polarizado::~Polarizado(){
}
void Polarizado::AplicarFiltro(){
    *getSaidaImagem() << getIdentificador() << endl;
    *getSaidaImagem() << getLargura() << " " << getAltura() << endl;
    *getSaidaImagem() << getMaximaCor() << endl;

    for(int i = 0; i < getAltura(); i++){
        for(int j = 0; j < getLargura(); j++){
            if((unsigned int)getPixel(i, j, 'r') < getMaximaCor()/2){
                setPixel(i, j, 'r', 0);
            }
            else{
                setPixel(i, j, 'r', (unsigned char)getMaximaCor());
            }
            if((unsigned int)getPixel(i, j, 'g') < getMaximaCor()/2){
                setPixel(i, j, 'g', 0);
            }
            else{
                setPixel(i, j, 'g', (unsigned char)getMaximaCor());
            }
            if((unsigned int)getPixel(i, j, 'b') < getMaximaCor()/2){
                setPixel(i, j, 'b', 0);
            }
            else{
                setPixel(i, j, 'b', (unsigned char)getMaximaCor());
            }
        }
    }
         for (int i=0;i<getAltura();i++){
       for(int j=0;j<getLargura();j++){
            *getSaidaImagem() << (unsigned char)getPixel(i, j, 'r');
            *getSaidaImagem() << (unsigned char)getPixel(i, j, 'g');
            *getSaidaImagem() << (unsigned char)getPixel(i, j, 'b');

       }

   }
    

}