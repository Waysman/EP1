#include <iostream>
#include <string>
#include <fstream>
//#include "imagem.hpp"
#include "filtro.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretobranco.hpp"
#include "media.hpp"
#include <stdlib.h>

using namespace std;

int main(){

    int menu;
    int verificador=0;
    
    do{
        
        cout << "Digite 1 para filtro negativo." << endl;
        cout << "Digite 2 para filtro polarizado." << endl;
        cout << "Digite 3 para filtro Preto e branco." << endl;
        cout << "Digite 4 para filtro de media." << endl;
        cout << "Digite 0 para sair." << endl;
        cout << "Digite a opcao desejada:" << endl;
        cin >> menu;
        switch(menu){
            case 1:
                Negativo * negativo;
                negativo = new Negativo();
                negativo->AplicarFiltro();
                verificador=1;

            break;
            case 2:
                Polarizado * polarizado;
                polarizado = new Polarizado();
                polarizado->AplicarFiltro();
                verificador=1;
            break;
            case 3:
                Pretobranco * pretobranco;
                pretobranco = new Pretobranco();
                pretobranco->AplicarFiltro();
                verificador=1;
            break;
            case 4:
                int size;
                cout << "Digite 1 para filtro 3x3." << endl;
                cout << "Digite 2 para filtro 5x5." << endl;
                cout << "Digite 3 para filtro 7x7." << endl;
                cout << "Digite a opcao desejada: ";
                cin >> size;
                if(size==1){
                    size=3;
                }
                else if(size==2){
                    size=5;
                }
                else if(size==3){
                    size=7;
                }
                else{
                    cout << "Opcao digitada invalida." << endl;
                    exit(1);
                }
                Media * media;
                media = new Media();
                media->AplicarFiltro(size);
                verificador=1;
            break;
            case 0: 
                cout << "Saindo do programa." << endl;
                verificador=1;
            break;
            default:
                cout << "Opcao digitada invalida." << endl;
                verificador=0;
            break;
        }
    }while(verificador==0);


    return 0;
}