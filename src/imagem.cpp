#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include "imagem.hpp"
#include <sstream>

using namespace std;
Imagem::Imagem(){

        nome="";
        identificador="";
        altura=0;
        largura=0;
        comentario="";
}
Imagem::~Imagem(){

}
std::string Imagem::getNome(){
    return nome;
}
void Imagem::setNome(std::string nome){
    this->nome=nome;
}
std::string Imagem::getIdentificador(){
    return identificador;
}
void Imagem::setIdentificador(std::string identificador){
    this->identificador=identificador;
}
int Imagem::getAltura(){
    return altura;
}
void Imagem::setAltura(int altura){
    this->altura=altura;
}
int Imagem::getLargura(){
    return largura;
}
void Imagem::setLargura(int largura){
    this->largura=largura;
}
unsigned int Imagem::getMaximaCor(){
    return maximacor;
}
void Imagem::setMaximaCor(int maximacor){
    this->maximacor=maximacor;
}

string Imagem::getComentario(){
    return comentario;
}
void Imagem::setComentario(string comentario){
    this->comentario=comentario;
}
unsigned char Imagem::getPixel(int i,int j, char cor){
        if(cor=='r'){
            return pixel[i][j].red;
        } 
        else if(cor=='g'){
            return pixel[i][j].green;
        } 
        else if(cor=='b'){
            return pixel[i][j].blue;
        }
        else{
            cout << "Erro em getPixel" << endl;
            return 0;
        }
}
void Imagem::setPixel(int i,int j, char cor, unsigned char valor){
        if(cor=='r'){
            pixel[i][j].red = valor;
        } 
        else if(cor=='g'){
            pixel[i][j].green = valor;
        } 
        else if(cor=='b'){
            pixel[i][j].blue = valor;
        }
        else{
            cout << "Erro em setPixel" << endl;
        }
}

void Imagem::lerArquivo(){
    
    string nome="";
    string conteudo="";
    string comentario="";
    string identificador="";
    string auxiliar="";
    int maximacor=0;
    int altura=0, largura=0;
    int contador=0;
    int verificador=0;
    int i=0, j=0;
    do{
        while(1){
            cout << "Digite o nome do arquivo: " << endl;
            cin >> auxiliar;
            nome="doc/" + auxiliar + ".ppm";
            Imagem::setNome(nome);

        ifstream imagementrada;
        imagementrada.open(Imagem::getNome().c_str(), ios::binary);

            if (!imagementrada){
                cout << "Erro ao abrir imagem" << endl;
                cout << "Digite novamente o nome do arquivo." << endl;
                cout << "Certifique-se de que o arquivo se encontra na pasta /doc/." << endl;
            }
            else{
                verificador=1;
                getline(imagementrada, identificador);
                if(identificador.compare("P6") != 0){
                    cout << "Tipo de Imagem invalido." << endl;
                    cout << "Por favor informe um arquivo de imagem valido, no formato .ppm" << endl;
                    imagementrada.close();
                }
                else{
                    while(contador==0){
        
    getline(imagementrada, comentario);
    if(comentario[0] != '#'){
        stringstream comentstream(comentario);
        comentstream >> largura;
        comentstream >> altura;
        contador=1;
    }
    
    
    }
   getline(imagementrada, auxiliar);
   stringstream maxcor(auxiliar);
   maxcor >> maximacor;

   Imagem::setIdentificador(identificador);
   Imagem::setLargura(largura);
   Imagem::setAltura(altura);
   Imagem::setMaximaCor(maximacor);
   
   
   
   
    pixel = new Pixel *[altura];
    for(i = 0; i < altura; i++) {
		pixel[i] = new Pixel [largura];
	}
    for(i = 0; i < altura; i++) {
		for(j = 0; j < largura; j++){
			imagementrada.get(pixel[i][j].red);
			imagementrada.get(pixel[i][j].green);
			imagementrada.get(pixel[i][j].blue);	
		}
	}


   
   imagementrada.close();
   break;
                }
                
            }
            
        }
    
        
        
    }while(verificador==0);
    
    
  

}
