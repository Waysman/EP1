# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

Compilar e executar.

No diretório do programa execute os comandos:
$ make clean
$ make
$ make run

Instruções de uso.

Ao executar o programa, certifique-se de que a imagem que deseja aplicar o filtro esteja na pasta ~/EP1/doc/

A nova imagem com filtro aplicado será criada na mesma pasta ~/EP1/doc/
