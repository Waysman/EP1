#include <iostream>
#include <string>
using namespace std;
class Imagem{
    public: //baseado no trabalho do icaro pires
    typedef struct pixel {
                char red;
                char green;
                char blue;
        } Pixel;
    private:
        std::string nome;
        std::string identificador;
        std::string comentario;
        int altura;
        int largura;
        int maximacor;
        Pixel **pixel;
    public:
        Imagem();
        ~Imagem();

        std::string getNome();
        void setNome(std::string nome);
        std::string getIdentificador();
        void setIdentificador(std::string identificador);
        int getAltura();
        void setAltura(int altura);
        int getLargura();
        void setLargura(int largura);
        unsigned int getMaximaCor();
        void setMaximaCor(int maximacor);

        std::string getComentario();
        void setComentario(std::string comentario);
        unsigned char getPixel(int i, int j, char cor);
	    void setPixel(int i, int j, char cor, unsigned char valor);

        void lerArquivo();
};