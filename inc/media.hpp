#ifndef MEDIA_HPP
#define MEDIA_HPP

#include "filtro.hpp"

class Media : public Filtro{
    private:
        int filter_size;
    public:
        Media();
        ~Media();
        void AplicarFiltro(int filter_size);
        void setFiltersize(int filter_size);
        int getFiltersize();

};

#endif