#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP
#include "filtro.hpp"

class Polarizado : public Filtro{

    public:
        Polarizado();
        ~Polarizado();
        void AplicarFiltro();

};

#endif