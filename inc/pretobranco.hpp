#ifndef PRETOBRANCO_HPP
#define PRETOBRANCO_HPP
#include "filtro.hpp"

class Pretobranco : public Filtro{
    public:
        Pretobranco();
        ~Pretobranco();
        void AplicarFiltro();

};

#endif