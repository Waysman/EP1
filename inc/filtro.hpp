#ifndef FILTRO_HPP
#define FILTRO_HPP
#include <iostream>
#include <string>
#include "imagem.hpp"

class Filtro : public Imagem{
    private:
        ofstream *imagemsaida;
    public:
    
        Filtro();
        //~Filtro();
        void AplicarFiltro();
        ofstream *getSaidaImagem();



};

#endif